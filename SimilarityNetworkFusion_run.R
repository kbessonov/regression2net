#this script runs SNF framework in the gene-gene context rather subject-subject
#Author: Kirill Bessonov
.dominateset <- function(xx,KK=20) {
  ###This function outputs the top KK neighbors.	
  
  zero <- function(x) {
    s = sort(x, index.return=TRUE)
    x[s$ix[1:(length(x)-KK)]] = 0
    return(x)
  }
  normalize <- function(X) X / rowSums(X)
  A = matrix(0,nrow(xx),ncol(xx));
  for(i in 1:nrow(xx)){
    A[i,] = zero(xx[i,]);
    
  }
  
  
  return(normalize(A))
}

SNF <- function(Wall,K=20,t=20) {
	
	###This function is the main function of our software. The inputs are as follows:
    # Wall : List of affinity matrices
    # K : number of neighbors
    # t : number of iterations for fusion
    
    ###The output is a unified similarity graph. It contains both complementary information and common structures from all individual network. 
    ###You can do various applications on this graph, such as clustering(subtyping), classification, prediction.
    
    LW = length(Wall)
    normalize <- function(X) X / rowSums(X)
    # makes elements other than largest K zero
    
    
    newW <- vector("list", LW)
    nextW <- vector("list", LW)
    ###First, normalize different networks to avoid scale problems.
    #for( i in 1: LW){
    #  Wall[[i]] = normalize(Wall[[i]]);
    #  Wall[[i]] = (Wall[[i]]+t(Wall[[i]]))/2;
    #}
    
    ### Calculate the local transition matrix.
    for( i in 1: LW){
      newW[[i]] = (.dominateset(Wall[[i]],K))
    }
    
    # perform the diffusion for t iterations
    for (i in 1:t) {
	  cat("iteration #",i," ...\n")
      for(j in 1:LW){
	  cat("Net #",j," ...\n")
        sumWJ = matrix(0,dim(Wall[[j]])[1], dim(Wall[[j]])[2])
        for(k in 1:LW){
          if(k != j) {
            sumWJ = sumWJ + Wall[[k]]
          }
        }
          nextW[[j]] = newW[[j]] %*% (sumWJ/(LW-1)) %*% t(newW[[j]]);
      }
      #Normalize each new obtained networks.
      #for(j in 1 : LW){
      #	      
      #          Wall[[j]] = nextW[[j]] + diag(nrow(Wall[[j]]));
      #          Wall[[j]] = (Wall[[j]] + t(Wall[[j]]))/2;
      #          }
   }
    
    # construct the combined affinity matrix by summing diffused matrices
    W = matrix(0,nrow(Wall[[1]]), ncol(Wall[[1]]))
    for(i in 1:LW){
      W = W + Wall[[i]]
    }
    W = W/LW;
    #W = normalize(W);
    #ensure affinity matrix is symmetrical
    W = (W + t(W)+diag(nrow(W))) / 2;
    
    #make sure we have only 0 and 1 entries and 1 on the diagonal
    #W = W/W[1,1]
    W[W>0] = 1
    return(W)  
  }

#require(SNFtool)
require(igraph) #SNFtool_2.2  and igraph_1.0.1
require(sna) # sna_2.3-2

load("last_ws.RData")
cat("Object loaded\n")
rownames(expr_expr)=colnames(expr_expr)
rownames(expr_meth)=colnames(expr_meth)

cat("Symmetrizing...\n")
n1ee = symmetrize(expr_expr, rule="weak")
n2em = symmetrize(expr_meth, rule="weak")

#diag(n1)=1; diag(n2)=1

print(date())
W = SNF(list(n1ee,n2em), K=20, t=1)
print(date())

cat("Saving data image...\n")
save.image("last_ws.RData")

#convert to graph structure
int_net = graph.adjacency(W, mode = "undirected")
degree_int = igraph::degree(int_net)


source("../RESULTS/BioAnotation/getGeneKEGGinfo.R")
#add code
genes_int.info <- getGenePathwayInfo(degree_int); 
genes_int.info[[2]] = KEGG.Bonferroni.correction.filter(genes_int.info[[2]]) 
write.table(genes_and.info[[2]], file="INT_net_KEGGInfoEnriched.txt", sep="\t", row.names=F, quote=F)

#Wraw=W  #copy of raw untouched similarity matrix

#diag(W)=0;  thr = quantile(W,0.99);
#W[W > thr]=1; W[W <= thr]=0;

cat("Saving image\n")



#and_net=upgrade_graph(and_net)
#xor_net=upgrade_graph(xor_net)

#ADJ_and_net=as.matrix(as_adj(and_net))
#ADJ_xor_net=as.matrix(as_adj(xor_net))

#diag(ADJ_and_net)=1; diag(ADJ_xor_net)=1

#print(date())
#W_and_net = SNF(list(ADJ_and_net,ADJ_xor_net),20,1)
#print(date())

#diag(W_and_net)=0;  thr = quantile(W_and_net,0.99);
#W_and_net[W_and_net > thr]=1; W_and_net[W_and_net <= thr]=0;




